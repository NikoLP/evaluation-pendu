![Logo pendu](./logo.svg)

# Le Pendu

Votre client souhaite apporter un côté fun au travail et vous demande de développer un jeu.

Le but de cette application va être de pouvoir jouer un jeu bien connu de tous _le pendu_.

> ⚠️ Il est recommandé comme toujours de bien lire l'intégralité du document avant de commencer ⚠️

## Glossaire

- Administrateur : personne qui, connectée, peut créer des parties
- Joueur : personne qui va jouer les parties des autres
- Jeu : entité qui contient le mot à deviner et un nombre de parties
- Partie : déclinaison du jeux pour chaque joueur

## Listes des fonctionnalités

### Côté admin

- Un administrateur peut s'inscrire et se connecter avec par exemple email/mot de passe
  - Tout le monde peut créer un compte administrateur
- Un administrateur peut créer un nouveau jeu en choisissant le mot qu'il veut faire deviner et la description du jeu
  - Chaque administrateur a ses propres jeux
- Un administrateur **ne peut pas** supprimer un jeu créé
- Un administrateur peut partager un code permettant de jouer à un jeu créé (un code unique, ça peut être l'uid)
- Un administrateur peut voir les jeux qu'il a déjà créé

#### Bonus

- Un administrateur peut choisir des indices pour son jeu lors de la création
- Un administrateur peut modifier les indices du jeu
- Un administrateur peut voir sur la liste des jeux créé les informations associées à ceux-ci
  - Est-ce que quelqu'un a terminés une partie ?
  - Est-ce que quelqu'un a réussi une partie ?

### Côté joueur

Pour les fonctionnalités _non-bonus_ il faudra que les endpoints de jeu côté API soient **publiques** et **non-authentifiés**.

- Un joueur peut jouer à un jeu précis en rentrant le code associé : il joue une partie d'un jeu
- Pendant la partie, le joueur pourra via un input tester des lettres pour cette partie
  - Si la lettre est dans le mot, elle s'affiche dans le résultat
  - Si elle n'est pas dans le mot, le compteur d'essai s'incrémente
- Un joueur visualise pendant sa partie la description du jeu
- Un joueur peut visualiser le nombre d'essai qu'il lui reste
- Un joueur peut voir les lettres qu'il a trouvées dans le résultat
- Un joueur doit savoir quand il a _gagné_ ou _perdu_ la partie

#### Bonus

- Un joueur peut jouer à un jeu _random_ tiré au hasard dans la base
- Pour identifier et faire des statistiques de réponse nous avons besoin d'identifier les joueurs. Nous ferons un système basé sur une empreinte générée à partir de choses uniques (machine, OS, navigateur etc...). Le joueur n'aura pas besoin de s'inscrire ni de se connecter manuellement.
  - Nous l'identifierons grâce à _l'empreinte_ navigateur [avec FingerprintJS par exemple](https://github.com/fingerprintjs/fingerprintjs) afin que l'id qu'on lui attribue soit unique mais pas aléatoire.
  - Il sera enregistré/connecté automatiquement via cet ID une fois généré (appel au backend avec cet id) au chargement de la page d'accueil
- Un joueur peut voir les jeux qu'il a commencés et non finis
- Dessiner un pendu en SVG sur le front en fonction du nombre dans le compteur d'essais d'une partie

## Relations

### Admin

- Un administrateur doit pouvoir se connecter, de la façon de votre choix, mais sécurisé.
- Il possède une liste de jeux associés

### Joueur

- Un joueur possède un `id` qui est une empreinte navigateur (voir plus haut).
- Un joueur peut jouer à plusieurs parties

### Jeu

- Un jeu possède un code unique pour permettre aux joueurs d'en faire des parties
- Un jeu possède un mot à faire deviner
- Un jeu possède un nombre **indéfini** de parties déclinées.
- Un jeu peut posséder une phrase de description courte, qui servira de **nom de jeu**
- Un jeu peut posséder des indices, 3 au maximum, qui sont textuels.

### Partie

- Une partie est une déclinaison d'un jeu associé à un joueur
- Une partie possède le nombre d'essai du joueur associé en cours
- Une partie possède les lettres essayées et le résultat en cours

## Attendus

Vous devrez développer l'intégralité de la stack nécessaire au fonctionnement de l'application à savoir :

- L'API pour accéder aux données
- La base de données
- Le front pour pouvoir jouer au jeu
  - Une section _admin_ pour l'administration
  - Une section _public_ pour les parties de jeu

Vous pouvez tout mettre dans le même dépot pour une gestion simplifiée du code (monorepo) en ayant un répertoire `server` et un répertoire `front` dans lequel vous aurez le code respectif.

## Notation

Comme d'habitude, on privilégie une application avec un peu moins de fonctionnalités mais qui fonctionne correctement plutôt qu'une application avec beaucoup de fonctionnalités non terminées.

La répartition de la notation est grossièrement de :

- 40% pour la partie back
- 40% pour la partie front
- 20% pour l'intégration continue, les livrables et les tests

Il n'y a pas de barême précis pour chaque tâche. Le cahier des charges est volontairement trop long pour la durée de l'évaluation, ce qui vous permet (en partie) de choisir sur quelles fonctionnalités travailler.

## Wireframe

![Wireframe pendu](./wireframe.png)
